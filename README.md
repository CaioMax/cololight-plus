# Cololight

[![Add To Installation](https://marketplace.signalrgb.com/resources/add-extension-256.png 'Add to My SignalRGB Installation')](signalrgb://addon/install?url=https://gitlab.com/signalrgb/cololight)

## Getting started
This is a simple SignalRGB extension for controlling cololight hex devices with SignalRGB.

## Known Issues
- No auth/encryption support
- No individual 'bead' support (yet)
- Only support for HEX devices (currently)

## Installation
Click the button above and allow signalrgb to install this extension when prompted.

## Support
Feel free to open issues here, or submit PRs.
